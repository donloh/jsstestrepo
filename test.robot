*** Settings ***
Library         lib/CloudShellAPILibrary.py 	${CloudShellAddress}	${Username}    ${Password}    ${Domain}    ${sandbox.id}
Library			SSHLibrary

Documentation   A test suite containing one test.
...             The suite should pass successfully.

Suite Setup            Add VM to Sandbox

*** Variables ***
${CloudShellAddress}    192.168.3.105
${Username}             admin
${Password}             admin
${Domain}               Global

*** Test Cases ***
Hello World
	Configure Server

*** Keywords ***
Add VM to Sandbox
    ${app} =			CloudShellAPILibrary.Add App To Reservation 	Ubuntu Server 20.04 LTS		VMware vCenter - vCenter VM From Linked Clone 2G
	${deployed_app} = 	CloudShellAPILibrary.Deploy App					${app}
	${ip} = 			CloudShellAPILibrary.Get Resource IP			${deployed_app}
	Set Suite Variable	${ip}		${ip}
	${user} =			CloudShellAPILibrary.Get Attribute Value		${deployed_app}		User
	Set Suite Variable	${user} 	${user}
	${pass} =			CloudShellAPILibrary.Get Attribute Value		${deployed_app}		Password
	Set Suite Variable	${pass} 	${pass} 

Configure Server
	SSHLibrary.Open Connection 	${ip}
	SSHLibrary.Login 			${user} 	${pass}
	${uname} = 					SSHLibrary.Execute Command  uname -a
	Log 						${uname}

from cloudshell.api.cloudshell_api import CloudShellAPISession, InputNameValue, SetConnectorRequest
from uuid import UUID
import re

class CloudShellAPILibrary(object):
    # def __init__(self, cloudshell_address, username, password, domain, sandbox_uuid: UUID =''):
    #     self.api_session = CloudShellAPISession(host=cloudshell_address, username=username, password=password, domain=domain)
    #     if sandbox_uuid:
    #         self.sandbox_id = str(sandbox_uuid)
    def __init__(self, cloudshell_address, user="admin", auth_token='', domain="Global", sandbox_uuid: UUID =""):
        self.api_session = CloudShellAPISession(cloudshell_address, user, token_id=auth_token, domain=domain)
        if sandbox_uuid:
            self.sandbox_id = str(sandbox_uuid)

    def write_sandbox_message(self, message):
        self.api_session.WriteMessageToReservationOutput(self.sandbox_id, message)

    def execute_command(self, resource, resource_type, command_name, command_params: dict = None):
        if command_params:
            api_params = [InputNameValue(param.name, param.value) for param in command_params.items()]
            output = self.api_session.ExecuteCommand(self.sandbox_id, resource, resource_type, command_name, api_params)
        else:
            output = self.api_session.ExecuteCommand(self.sandbox_id, resource, resource_type, command_name)

        return output.Output

    def get_reservation_details(self):
        reservation_details = self.api_session.GetReservationDetails(self.sandbox_id)
        return reservation_details.ReservationDescription.__dict__

    def get_reservation_id(self, sandbox_name):
        reservation_id = [reservation.Id for reservation in self.api_session.GetCurrentReservations().Reservations if sandbox_name in reservation.Name][0]
        return reservation_id

    def add_app_to_reservation(self, appName, deploymentPath, positionX=100, positionY=100):
        output = self.api_session.AddAppToReservation(reservationId=self.sandbox_id, appName=appName, deploymentPath=deploymentPath, positionX=positionX, positionY=positionY)
        return output.ReservedAppName

    def set_connectors_in_reservation(self, SourceResourceFullName, TargetResourceFullName):
        connectRequest = SetConnectorRequest(SourceResourceFullName=SourceResourceFullName, TargetResourceFullName=TargetResourceFullName, Direction='bi', Alias=None)
        self.api_session.SetConnectorsInReservation(self.sandbox_id, [connectRequest])

    def deploy_app(self, resource):
        output = self.api_session.ExecuteCommand(self.sandbox_id, resource, "App", "deploy").Output
        output = re.findall(r'"([^"]*)"', output)[0]
        return output

    def get_resource_ip(self, resourceName):
        output = self.api_session.GetResourceDetails(resourceName, False)
        return output.FullAddress
        
    def get_attribute_value(self, resourceName, attributeName): 
        output = self.api_session.GetAttributeValue(resourceName, attributeName)
        if attributeName == 'Password':
            output = self.api_session.DecryptPassword(output.Value)
        return output.Value
